package handler

import (
	"context"
	"encoding/json"

	"github.com/micro/go-log"

	api "github.com/micro/go-api/proto"
	"github.com/micro/go-micro/errors"
	"gitlab.com/fhdo-ewks2018/go/containerservice/api/client"
	container "gitlab.com/fhdo-ewks2018/go/containerservice/srv/proto/container"
)

type Container struct{}

func extractValue(pair *api.Pair) string {
	if pair == nil {
		return ""
	}
	if len(pair.Values) == 0 {
		return ""
	}
	return pair.Values[0]
}

// Container.GetItems is called by the API as /container/container/getitems with post body {"userid": "foo"}
func (e *Container) GetItems(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Log("Received Container.GetItems request")
	log.Log("Method: " + req.Method + "\nPath: " + req.Path + "\nBody: " + req.Body + "\nUrl: " + req.Url)

	// extract the client from the context
	containerClient, ok := client.ContainerFromContext(ctx)
	if !ok {
		return errors.InternalServerError("de.fhdortmund.fb4.ewks.api.container.getitems", "container client not found")
	}

	// make request
	response, err := containerClient.GetItems(ctx, &container.Request{
		UserId: extractValue(req.Get["userid"]),
	})
	if err != nil {
		return errors.InternalServerError("de.fhdortmund.fb4.ewks.api.container.getitems", err.Error())
	}

	b, _ := json.Marshal(response)

	rsp.StatusCode = 200
	rsp.Body = string(b)

	return nil
}
