package main

import (
	"github.com/micro/go-log"

	"github.com/micro/go-micro"
	"gitlab.com/fhdo-ewks2018/go/containerservice/api/client"
	"gitlab.com/fhdo-ewks2018/go/containerservice/api/handler"

	container "gitlab.com/fhdo-ewks2018/go/containerservice/api/proto/container"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("de.fhdortmund.fb4.ewks.api.container"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init(
		// create wrap for the Container srv client
		micro.WrapHandler(client.ContainerWrapper(service)),
	)

	// Register Handler
	container.RegisterContainerHandler(service.Server(), new(handler.Container))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
