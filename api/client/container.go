package client

import (
	"context"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	container "gitlab.com/fhdo-ewks2018/go/containerservice/srv/proto/container"
)

type containerKey struct{}

// FromContext retrieves the client from the Context
func ContainerFromContext(ctx context.Context) (container.ContainerService, bool) {
	c, ok := ctx.Value(containerKey{}).(container.ContainerService)
	return c, ok
}

// Client returns a wrapper for the ContainerClient
func ContainerWrapper(service micro.Service) server.HandlerWrapper {
	client := container.NewContainerService("de.fhdortmund.fb4.ewks.srv.container", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, containerKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
