package model

import (
	"github.com/micro/go-log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Container struct {
	Id          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	CharacterId string        `bson:"characterId"`
	Name        string        `bson:"name"`
	WeightLimit float32       `bson:"weightLimit"`
	Items       []Item        `bson:"items"`
}

type Item struct {
	ItemId string `bson:"_id,omitempty"`
	Amount uint32 `bson:"amount"`
}

type DatabaseConf struct {
	session    *mgo.Session
	collection *mgo.Collection
}

var examples = []Container{
	Container{
		bson.ObjectId(""), "12345", "Backpack", 32.0, []Item{Item{"12", 12}, Item{"1", 5}},
	},
	Container{
		bson.ObjectId(""), "12345", "Quiver", 40.0, []Item{Item{"1", 1}},
	},
}

func (db *DatabaseConf) InsertExampleContainers() {
	log.Log("InsertExampleContainers() called")

	for _, example := range examples {
		err := db.collection.Insert(example)

		if err != nil {
			log.Log("Insert error: " + err.Error())
		}
	}
}

func (db *DatabaseConf) Find(characterId string) []Container {
	var result []Container

	err := db.collection.Find(bson.M{"characterId": characterId}).All(&result)
	if err != nil {
		log.Log("Insert error: " + err.Error())
	}

	return result
}

func (db *DatabaseConf) Init(url, database string) {
	log.Log("Init() called")

	session, err := mgo.Dial(url)
	if err != nil {
		panic(err)
	}

	db.session = session
	db.collection = session.DB(database).C("container")

	db.session.SetSafe(&mgo.Safe{})
}

func (db *DatabaseConf) Close() {
	log.Log("Close() called")

	db.session.Close()
}
