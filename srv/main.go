package main

import (
	"github.com/micro/go-log"
	"github.com/micro/go-micro"
	"gitlab.com/fhdo-ewks2018/go/containerservice/srv/handler"

	model "gitlab.com/fhdo-ewks2018/go/containerservice/srv/model"
	container "gitlab.com/fhdo-ewks2018/go/containerservice/srv/proto/container"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("de.fhdortmund.fb4.ewks.srv.container"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Create Database model and connect to mongo
	model := new(model.DatabaseConf)
	model.Init("localhost", "test")
	defer model.Close()

	//model.InsertExampleContainers()

	// Register Handler
	handler := new(handler.Container)
	handler.Init(model)
	container.RegisterContainerHandler(service.Server(), handler)

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
