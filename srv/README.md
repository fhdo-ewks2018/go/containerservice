# Container Service

This is the Container service

Generated with

```
micro new gitlab.com/fhdo-ewks2018/go/containerservice/srv --namespace=de.fhdortmund.fb4.ewks --alias=container --type=srv --plugin=registry=eureka
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: de.fhdortmund.fb4.ewks.srv.container
- Type: srv
- Alias: container

## Dependencies

Micro services depend on service discovery. The default is consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
MICRO_REGISTRY=eureka MICRO_REGISTRY_ADDRESS=http://localhost:8888/eureka/v2 ./container-srv
```

Build a docker image
```
make docker
```
