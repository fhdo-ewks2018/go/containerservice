package handler

import (
	"context"

	"github.com/micro/go-log"

	model "gitlab.com/fhdo-ewks2018/go/containerservice/srv/model"
	container "gitlab.com/fhdo-ewks2018/go/containerservice/srv/proto/container"
)

type Container struct {
	db *model.DatabaseConf
}

func (e *Container) Init(model *model.DatabaseConf) {
	e.db = model
}

// GetItems is a single request handler called via client.GetItems or the generated client code
func (e *Container) GetItems(ctx context.Context, req *container.Request, rsp *container.Response) error {
	log.Log("Received Container.GetItems request for: " + req.UserId)

	result := e.db.Find(req.UserId)

	for _, element := range result {
		var tmpItems []*container.Response_Container_Item
		for _, item := range element.Items {
			tmpItems = append(tmpItems, &container.Response_Container_Item{ItemId: item.ItemId,
				Amount: item.Amount})
		}

		rsp.Containers = append(rsp.Containers, &container.Response_Container{ContainerId: element.Id.Hex(),
			Name:        element.Name,
			WeightLimit: element.WeightLimit,
			Items:       tmpItems})
	}

	return nil
}
