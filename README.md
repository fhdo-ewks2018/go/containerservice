# Container Repository

This is the repository for the Container micorservices.
Build with [micro](https://micro.mu/)

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

## Dependencies
The package requires a running *mongodb* and *eureka* server.
The best way to get both up and running is via docker.

Additionally install *protobuf* from your package manager.

The following package are dependencies for the container services.
* github.com/micro/micro
* github.com/golang/protobuf/{proto,protoc-gen-go}
* github.com/micro/protoc-gen-micro
* github.com/micro/go-plugins/registry/eureka

And can be installed with the following commands:
```
go get -u github.com/micro/micro
go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go get -u github.com/micro/protoc-gen-micro
go get -u github.com/micro/go-plugins/registry/eureka
```

### Eureka wit Micro
To use the API Gateway from the Micro package, you need to compile the eureka support into the micro binary.

Copy one of the *plugin.go* from either the *api/* or *srv/* directory into *$GOPATH/src/github.cim/micro/micro* and run:
```
go build -o micro main.go plugin.go
```

And copy the resulting *micro* binary into the *$GOPATH/bin/* directory.

## Usage
To use the binaries, first compile them, then execute them.

### Compiling
First, add *$GOPATH/bin* to your PATH:
```
export PATH="$PATH:$GOPATH/bin"
```

Then go into the *srv/* directory and execute:
```
make build
```

After that go into the *api/* directory and execute the command again:
```
make build
```

### Execution
To run the service, you first have to set some environment variables:
```
export MICRO_API_NAMESPACE=de.fhdortmund.fb4.ewks.api MICRO_REGISTRY=eureka MICRO_REGISTRY_ADDRESS=http://localhost:8888/eureka/v2 MICRO_REGISTER_TTL=30 MICRO_REGISTER_INTERVAL=10
```

Then run the service by running both executables:
```
./srv/container-srv &
./api/container-api &
```

Finally start the API Gateway from the modified *micro* executable:
```
micro api
```
